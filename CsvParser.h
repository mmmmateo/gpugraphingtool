#ifndef ALLEGROSEARCH_CSVPARSER_H
#define ALLEGROSEARCH_CSVPARSER_H

#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include "GraphicsCard.h"
#include "Filters.h"
#include "FlagsValue.h"

std::vector<GraphicsCard> loadGPUData(const std::string& filename, FlagsValues *flags) {
    std::vector<GraphicsCard> gpuData;

    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cout << "Failed to open file: " << filename << std::endl;
        return gpuData;
    }

    std::string line;
    std::getline(file, line);

    while (std::getline(file, line)) {


        std::stringstream ss(line);
        std::string name, ram, test, brand;
        double performance1440p, performance4K, price;
        int tdp;

        std::getline(ss, brand, ' ');
        std::getline(ss, name, ';');
        ss >> performance1440p;
        ss.ignore(1);
        ss >> performance4K;
        ss.ignore(1);
        std::getline(ss, ram, ';');
        ss >> tdp;
        ss.ignore(2);
        ss >> price;

        if(filterBrand(brand, flags) and filterRam(ram, flags)) gpuData.push_back({ name, performance1440p, performance4K, ram, tdp, price });
    }

    file.close();
    return gpuData;
}
#endif //ALLEGROSEARCH_CSVPARSER_H
