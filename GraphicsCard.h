#ifndef ALLEGROSEARCH_GRAPHICSCARD_H
#define ALLEGROSEARCH_GRAPHICSCARD_H

#include <string>

struct GraphicsCard {
    std::string name;
    double performance1440p;
    double performance4K;
    std::string ram;
    int tdp;
    double price;
};

void printGpuData(GraphicsCard gpu){
    std::cout << "Name: " << gpu.name << std::endl;
    std::cout << "Performance (1440p): " << gpu.performance1440p << std::endl;
    std::cout << "Performance (4K): " << gpu.performance4K << std::endl;
    std::cout << "RAM: " << gpu.ram << std::endl;
    std::cout << "TDP: " << gpu.tdp << std::endl;
    std::cout << "Price: " << gpu.price << std::endl;
    std::cout << std::endl;
}
#endif //ALLEGROSEARCH_GRAPHICSCARD_H
