#include <iostream>
#include <vector>
#include <matplot/matplot.h>
#include <algorithm>
#include "GraphicsCard.h"
#include "FlagsValue.h"
#include "commandParser.h"
#include "CsvParser.h"


double highestPrice = 0;
int highestPower = 0;
FlagsValues flags;

void drawGraph(std::vector<std::string> names, std::vector<std::vector<double>> comparison){
    matplot::bar(comparison);
    matplot::gca()->x_axis().ticklabels(names);
    matplot::gca()->x_axis().tickangle(45);

//    matplot::xticklabels(names);
//    matplot::xtickangle(80);
//    matplot::show();
}

int main(int argc, char* argv[]) {
    commandParser(argc, argv, &flags);

    //Set default falgs
    if(flags.res4kFlag == 0 and flags.res2kFlag == 0) flags.res4kFlag = 1;
    if(flags.amdFlag == 0 and flags.nvidiaFlag == 0) {
        flags.nvidiaFlag = 1;
        flags.amdFlag = 1;
    }

    std::vector<GraphicsCard> gpuData = loadGPUData(flags.filename, &flags);
    if(gpuData.empty()){
        std::cout << "Wrong filename or empty file.\n";
        return 1;
    }

    //find the highest price and power
    for (const auto& gpu : gpuData) {
        highestPrice = std::max(highestPrice, gpu.price);
        highestPower = std::max(highestPower, gpu.tdp);
    }

    std::vector<std::string> names;
    std::vector<std::vector<double>> comparison;
    std::vector<double> performance;
    std::vector<double> performanceNormalized;
    std::vector<double> performanceNormalizedWithPowerUsage;

    double usagePriceForW = flags.hoursUsage * flags.priceForkWh / 1000;

    if(flags.res4kFlag == 1){
        for (const auto& gpu : gpuData) {
            names.push_back(gpu.name);
            performance.push_back(gpu.performance4K / 100);
            performanceNormalized.push_back(gpu.performance4K * highestPrice / gpu.price / 100);

            performanceNormalizedWithPowerUsage.push_back(gpu.performance4K * (highestPrice + highestPower * usagePriceForW ) / (gpu.price + gpu.tdp * usagePriceForW ) / 100);
        }
        comparison = {performance, performanceNormalized, performanceNormalizedWithPowerUsage};

        drawGraph(names, comparison);
        matplot::save("img/gpu4k.jpg");
    }


    if(flags.res2kFlag == 1){
        for (const auto& gpu : gpuData) {
            names.push_back(gpu.name);
            performance.push_back(gpu.performance1440p / 100);
            performanceNormalized.push_back(gpu.performance1440p * highestPrice / gpu.price / 100);

            performanceNormalizedWithPowerUsage.push_back(gpu.performance1440p * (highestPrice + highestPower * usagePriceForW ) / (gpu.price + gpu.tdp * usagePriceForW ) / 100);
        }
        comparison = {performance, performanceNormalized, performanceNormalizedWithPowerUsage};

        drawGraph(names, comparison);
        matplot::save("img/gpu2k.jpg");
    }

    return 0;
}

