#ifndef ALLEGROSEARCH_FLAGSVALUE_H
#define ALLEGROSEARCH_FLAGSVALUE_H

#include <string>

struct FlagsValues {
    double hoursUsagePerWeek = 5;
    double yearsUsage = 5;
    double hoursUsage = yearsUsage * hoursUsagePerWeek * 365 / 7;

    int minRam = 0;
    double priceForkWh = 0.77;
    std::string filename = "./gpu1.csv";

    int amdFlag = 0;
    int nvidiaFlag = 0;
    int res2kFlag = 0;
    int res4kFlag = 0;
};
#endif //ALLEGROSEARCH_FLAGSVALUE_H
