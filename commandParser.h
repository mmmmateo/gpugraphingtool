#ifndef ALLEGROSEARCH_COMMANDPARSER_H
#define ALLEGROSEARCH_COMMANDPARSER_H

#include <iostream>
#include "FlagsValue.h"

void commandParser(int argc, char* argv[], FlagsValues *flags){
    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if(arg == "-h" or arg == "-help"){
            std::cout << "Usage: <command> [options]\n"
                      << "Options:\n"
                      << "-h, -help             Display this help message\n"
                      << "-4k                   Enable 4K resolution\n"
                      << "-2k                   Enable 2K resolution\n"
                      << "-n, -nvidia           Enable Nvidia GPU\n"
                      << "-a, -amd              Enable AMD GPU\n"
                      << "-r, -ram <value>      Set minimum RAM value\n"
                      << "-w, -weekUsage <value> Set weekly usage in hours\n"
                      << "-y, -yearUsage <value> Set yearly usage in hours\n"
                      << "-W, -priceForkWh <value> Set price per kWh\n"
                      << "-f, -file <filename>  Set filename for output\n";
        }else if(arg == "-4k"){
            flags->res4kFlag = 1;
        }else if(arg == "-2k"){
            flags->res2kFlag = 1;
        }else if(arg == "-n" or arg == "-nvidia"){
            flags->nvidiaFlag = 1;
        }
        else if(arg == "-a" or arg == "-amd"){
            flags->amdFlag = 1;
        }else if(arg == "-r" or arg == "-ram"){
            if (i + 1 < argc) {
                try {
                    flags->minRam = std::stoi(argv[i + 1]);
                } catch (const std::exception& e) {
                    std::cout << "Invalid argument for integer flag.\n";
                }
            }
            else {
                std::cout << "Missing argument for integer flag.\n";
            }
        }else if(arg == "-w" or arg == "-weekUsage"){
            if (i + 1 < argc) {
                try {
                    flags->hoursUsagePerWeek = std::stod(argv[i + 1]);
                } catch (const std::exception& e) {
                    std::cout << "Invalid argument for week hours usage flag.\n";
                }
            }
            else {
                std::cout << "Missing argument for double flag.\n";
            }
        }else if(arg == "-y" or arg == "-yearUsage"){
            if (i + 1 < argc) {
                try {
                    flags->yearsUsage = std::stod(argv[i + 1]);
                } catch (const std::exception& e) {
                    std::cout << "Invalid argument for years usage flag.\n";
                }
            }
            else {
                std::cout << "Missing argument for double flag.\n";
            }
        }
        else if(arg == "-W" or arg == "-priceForkWh"){
            if (i + 1 < argc) {
                try {
                    flags->priceForkWh = std::stod(argv[i + 1]);
                } catch (const std::exception& e) {
                    std::cout << "Invalid argument for price for kWh usage flag.\n";
                }
            }
            else {
                std::cout << "Missing argument for double flag.\n";
            }
        }else if(arg == "-f" or arg == "-file"){
            if (i + 1 < argc) {
                flags->filename = argv[i + 1];
            }
            else {
                std::cout << "Missing argument for file flag.\n";
            }
        }
    }
}

#endif //ALLEGROSEARCH_COMMANDPARSER_H
