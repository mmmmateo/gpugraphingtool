#ifndef ALLEGROSEARCH_FILTERS_H
#define ALLEGROSEARCH_FILTERS_H

#include <string>
#include "FlagsValue.h"

bool filterBrand(const std::string& brand, FlagsValues *flags){
    if(flags->nvidiaFlag == 1 and brand == "GeForce") return true;
    if(flags->amdFlag == 1 and brand == "Radeon") return true;
    return false;
}

bool filterRam(const std::string& ram, FlagsValues *flags){
    if(std::stoi(ram) > flags->minRam) return true;
    return false;
}
#endif //ALLEGROSEARCH_FILTERS_H
